# -*- coding: utf-8 -*-
import json

import datetime
import threading

import requests, shutil, random, string, os
from bs4 import BeautifulSoup
from slugify import slugify

CATEGORY_CREATE_URL = 'http://82.146.43.204/categories/create/'
PRODUCT_CREATE_URL = 'http://82.146.43.204/products/create/'


def get_html(url):
    r = requests.get(url)
    return r.text if r.status_code == 200 else None


def parse_categories(html):
    categories = list()
    soup = BeautifulSoup(html, 'html.parser')
    categories_container = soup.find('ul', id='gtm-catalog').find_all('li', class_='submenu', recursive=False)

    for p_item in categories_container:
        p_cat = p_item.find('a', recursive=False)
        first_category = requests.post(CATEGORY_CREATE_URL, data={
            'title': p_cat.text
        })
        for s_item in p_item.find('div', class_='navbar-1').find('ul', class_='navbar-1__nav').find_all('li',
                                                                                                        class_='submenu',
                                                                                                        recursive=False):
            s_cat = s_item.find('a', recursive=False)
            second_category = requests.post(CATEGORY_CREATE_URL, data={
                'title': s_cat.text,
                'parent_id': json.loads(first_category.text)['id']
            })
            categories.append(dict(id=json.loads(second_category.text)['id'], url=s_cat.get('href')))
            for t_item in s_item.find('div', class_='navbar-2').find('ul', class_='navbar-2__nav').find_all('li',
                                                                                                            class_='nav-item',
                                                                                                            recursive=False):
                t_cat = t_item.find('a', recursive=False)
                third_category = requests.post(CATEGORY_CREATE_URL, data={
                    'title': t_cat.text,
                    'parent_id': json.loads(second_category.text)['id']
                })

                print(dict(id=json.loads(third_category.text)['id'], url=t_cat.get('href')))

                categories.append(dict(id=json.loads(third_category.text)['id'], url=t_cat.get('href')))

    return categories


def parse_category_page(html, cat_id):
    if html:
        soup = BeautifulSoup(html, 'html.parser')
        try:
            product_container = soup.find('div', id='category-product').find('div', class_='result-search').find_all(
                'div',
                class_='product-card')
        except:
            product_container = []

        pagination = soup.find('div', class_='result-sort--pagination')


        for p in product_container:
            link = p.find('div', class_='product-card__name').find('a').get('href')
            thread = threading.Thread(target=parse_single_product_page, args=(get_html(link), cat_id))
            thread.start()


        if pagination:
            pages = pagination.find('nav').find('ul', class_='pagination', recursive=False).find_all('li')
            pages = [x.find('a', class_='next') for x in pages if x.find('a', class_='next')]
            next_page = pages[0] if len(pages) > 0 else None
            if next_page:
                _thread = threading.Thread(target=parse_category_page, args=(
                    get_html(next_page.get('href')),
                    cat_id
                ))
                _thread.start()




def parse_single_product_page(html, cat_id):
    try:
        soup = BeautifulSoup(html, 'html.parser')
    except:
        return
    try:
        article = soup.find('span', class_='product-sku-num').text.strip()
    except:
        article = random.randint(1000000, 9999999)
    try:
        title = soup.find('h1', class_='product-header').text.strip()
    except:
        title = 'Unknown product title'
    try:
        price = float(soup.find('div', id='price-less-10').find('div', class_='price-cur').find('span',
                                                                                                class_='num').text.replace(
            ',', '.').strip().replace(' ', ''))
    except:
        price = 0
    try:
        old_price_container = soup.find('div', id='price-less-10').find('div', class_='price-old')
    except:
        old_price_container = None
    discount = 0
    if old_price_container:
        old_price = float(old_price_container.text.strip().split()[0].replace(',', '.').replace(' ', ''))
        percent_value = old_price - price
        discount = float((percent_value / old_price) * 100)

    brand = None
    try:
        characteristics_container = soup.find('div', id='product-1-inner').find('div',
                                                                                class_='collapse-list-content').find(
            'div', class_='product-spec-wrap').find_all('div', class_='product-spec__item')
    except:
        characteristics_container = []
    characteristics = list()
    for c_item in characteristics_container:
        holder = c_item.find('dl', class_='product-spec')
        key = holder.find('dt', class_='product-spec__name').text.strip()
        value = holder.find('dd', class_='product-spec__value').text.strip()
        if slugify(key.lower()) != slugify('бренд'):
            characteristics.append(dict(
                key=key.lower(),
                value=value
            ))
        else:
            brand = value

    try:
        description = soup.find('div', id='product-2-inner').find('div', class_='collapse-list-content').find(
            'div').find(
            'noindex').text
    except:
        description = ""

    documentation_container = soup.find('div', id='product-3')
    documentation = None
    if documentation_container:
        tab_title = documentation_container.find('a', class_='tab-link-collaps', recursive=False).text.strip()
        if slugify(tab_title) == slugify('документация'):
            documentation = documentation_container.find(
                'div',
                class_='collapse-list',
                recursive=False
            ).find(
                'div',
                class_='collapse-list-content',
                recursive=False
            ).find(
                'div',
                recursive=False
            ).text

    images = ()
    try:
        img_container = soup.find('div', class_='product-summary-slider').find_all('div', class_='slider-item')
        for s in img_container:
            img = download_image(s.find('img').get('src'))
            if img:
                images += img,
    except:
        pass

    product = dict(
        article=article,
        title=title,
        price=price,
        discount=discount,
        brand=brand,
        characteristics=characteristics,
        description=description,
        documentation=documentation,
        images=images
    )

    print(product)

    files = list()
    if len(product['images']) > 0:
        for img in product['images']:
            files.append(('files', open(img, 'rb')))

    kwargs = dict(data={
        'title': product['title'],
        'price': product['price'],
        'discount': product['discount'],
        'brand': product['brand'],
        'characteristics': json.dumps(product['characteristics']),
        'description': product['description'],
        'documentation': product['documentation'],
        'category_id': cat_id,
        'article': product['article']
    })

    if len(files) > 0:
        kwargs['files'] = files

    r = requests.post(PRODUCT_CREATE_URL, **kwargs)

    if os.path.exists('logs/process.log'):
        open_role = 'a'
    else:
        open_role = 'w'

    with open('logs/process.log', open_role) as f:
        f.write(
            """
            =================================================================================================
            Product uploaded, server message: %s; \n
            Slug: %s; \n
            Category_id: %s; \n
            images: [%s]; \n
            uploading date: %s;
            =================================================================================================
            """ % (
                r.text,
                slugify(product['title']),
                cat_id,
                ', '.join(product['images']),
                str(datetime.datetime.now().__format__('%d %b %Y %H:%M'))
            )
        )
        f.close()

    for f in files:
        f[1].close()

    for i in product['images']:
        os.remove(i)


def download_image(url):
    img = None

    if not os.path.exists('img'):
        os.mkdir('img')

    r = requests.get(url, stream=True)
    file_name = '%s.jpg' % ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))
    file_path = 'img/%s' % file_name
    if r.status_code == 200:
        with open(file_path, 'wb') as f:
            r.raw.decode_content = True
            shutil.copyfileobj(r.raw, f)
            img = file_path
    return img


def main():
    if not os.path.exists('logs'):
        os.mkdir('logs')

    categories = parse_categories(get_html('https://isolux.ru/'))

    for cat in categories:
        parse_category_page(get_html(cat['url']), cat['id'])


if __name__ == '__main__':
    main()
