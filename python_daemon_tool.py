#!/var/www/parser/isolux_parser/.env/bin/python3

import sys, time

import os

from python_daemon import Daemon
from parser import main
import traceback
import logging


class ParserDaemon(Daemon):
    def run(self):
        while True:
            try:
                main()
            except Exception as e:
                logging.error(traceback.format_exc())
                open_mode = 'a'
                if not os.path.exists('logs'):
                    os.mkdir('logs')
                    open_mode = 'w'

                with open('logs/cmd_logs.log', open_mode) as f:
                    f.write(e.__traceback__.__str__())
                    f.close()


if __name__ == "__main__":
    daemon = ParserDaemon('/tmp/isolux_parser.pid')
    if len(sys.argv) == 2:
        if 'start' == sys.argv[1]:
            daemon.start()
        elif 'stop' == sys.argv[1]:
            daemon.stop()
        elif 'restart' == sys.argv[1]:
            daemon.restart()
        else:
            print()
            "Unknown command"
            sys.exit(2)
        sys.exit(0)
    else:
        print("usage: %s start|stop|restart" % sys.argv[0])
        sys.exit(2)
